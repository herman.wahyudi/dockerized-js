# Overview #

The simple hello dockerized project for nestJS and AngularJS

## Clone & Build the src ##

1. git clone https://gitlab.com/herman.wahyudi/dockerized-js.git
2. docker login (use your docker hub account)
---
1. cd dockerized-js/nestjs/hello-nestjs
2. docker build -t hello-nestjs:1.0.0 .
3. docker tag hello-nestjs:1.0.0 hermanwahyudi/hello-nestjs:1.0.0
4. docker push hermanwahyudi/hello-nestjs:1.0.0
---
1. cd dockerized-js/angularjs/hello-ng
2. docker build -t hello-angularjs:1.0.0 .
3. docker tag hello-angularjs:1.0.0 hermanwahyudi/hello-angularjs:1.0.0
4. docker push hermanwahyudi/hello-angularjs:1.0.0

## How to Run for nestJS ##

1. docker login (use your docker hub account)
2. docker run -d -p 3000:3000 hermanwahyudi/hello-nestjs:1.0.0
3. curl localhost:3000

## How to Run for AngularJS ##

1. docker login (use your docker hub account)
2. docker run -d -p 80:80 hermanwahyudi/hello-angularjs:1.0.0
3. curl localhost

### The image size by multi-stage pov ###
> hermanwahyudi/hello-nestjs      1.0.0     defcaab0e255   55 minutes ago      182MB  
> hermanwahyudi/hello-angularjs   1.0.0     3f2db141a470   59 minutes ago      40.9MB  